import {Directive, ElementRef, HostListener, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appStyle]'
})
export class StyleDirective {
  @Input() dStyles: {background: string, borderRadius: string}

  constructor (private el: ElementRef, private r: Renderer2) {}

  @HostListener('mouseenter') onEnter() {
    this.r.setStyle(this.el.nativeElement, 'background', this.dStyles.background)
    this.r.setStyle(this.el.nativeElement, 'border-radius', this.dStyles.borderRadius)
  }

  @HostListener('mouseleave') onLeave() {
    this.r.setStyle(this.el.nativeElement, 'background', null)
    this.r.setStyle(this.el.nativeElement, 'border-radius', null)
  }

}
