import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MyValidators} from "./my.validators";
import {LocalStorageService} from "./services/local-storage.service";

export interface TaskStructure {
  id: number
  text: string
  status: boolean
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  isEditMode: boolean = false
  keyLS = 'dataFromLS'
  form: FormGroup

  constructor(protected localStorageService: LocalStorageService) {}

  ngOnInit() {
    this.form = new FormGroup({
      newTask: new FormControl('', [
        Validators.required,
        MyValidators.spaceOnly
      ]),
      taskID: new FormControl('')
    })

    this.localStorageService.getData(this.keyLS)
  }

  submit() {
    if (this.form.valid) {
      this.localStorageService.saveTask(this.keyLS, this.form.get('newTask').value)
      this.form.reset()
    }
  }

  updateTask(newText: string, id: string) {
    this.localStorageService.updateTask(this.keyLS, newText, id)
    this.form.reset()
  }
}
