import {FormControl} from "@angular/forms";

export class MyValidators {
  static spaceOnly(control: FormControl): { [key: string]: boolean } | null {
    const value = control.value;
    if (typeof value === 'string') {
      const str = value.trim();
      if (str.length === 0) {
        return { spaceOnly: true };
      }
    }
    return null;
  }
}
