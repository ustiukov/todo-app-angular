import {Injectable} from '@angular/core';


export interface TaskStructure {
  id: number
  text: string
  status: boolean
}

@Injectable({
  providedIn: 'root'
})

export class LocalStorageService {
  taskData: TaskStructure[] = []
  hasTaskData: boolean = true

  // isDataExist(key: string): boolean {
  //   if (localStorage.getItem(key)) return true
  // }

  saveTask(key: string, text: string) {
    const newID: number = this.getId()
    this.taskData.unshift({id: newID, text: text, status: true})
    this.saveToLocalStorage(key, this.taskData)
    this.hasTaskData = true
  }

  getData(key: string) {
    // Если данные существуют или удалены
    if (localStorage.getItem(key)) {
      this.taskData = JSON.parse(localStorage.getItem(key))
      if (!this.taskData.length) this.hasTaskData = false
    } else {
      // Если данных нет
      this.saveTask(key, 'Попросить кота поймать мышь')
      this.saveTask(key, 'Купить кота')
      this.saveTask(key, 'Продать мышеловку')
    }
  }

  removeTask(key: string, id: string) {
    this.taskData.splice(this.taskData.findIndex(el => el.id === parseInt(id, 10)), 1)
    if (!this.taskData.length) this.hasTaskData = false
    this.saveToLocalStorage(key, this.taskData)
  }

  taskCompleted(key: string, id: string) {
    this.taskData.find(el => el.id === parseInt(id, 10)).status =
      !this.taskData.find(el => el.id === parseInt(id, 10)).status
    this.saveToLocalStorage(key, this.taskData)
  }

  updateTask(key: string, newText: string, id: string) {
    this.taskData.find(el => el.id === parseInt(id, 10)).text = newText
    this.saveToLocalStorage(key, this.taskData)
  }

  getId(): number {
    const idList: Set<number> = new Set() // массив для хранения id строк
    // Подгрузить существующие id при добавлении новой задачи
    this.taskData.forEach((task) => idList.add(task.id))
    let newId = 0
    while (idList.has(newId)) newId++
    return newId
  }

  saveToLocalStorage(key: string, data: object[]) {
    localStorage.setItem(key, JSON.stringify([...data]))
  }
}
